﻿using CollectionsAndLinq.Interfaces;
using CoolParking.ConsoleApp.Helpers;
using System;
using System.Drawing;
using System.Threading.Tasks;

namespace CollectionsAndLinq
{
    public class CollectionAndLinqConsole : ICollectionAndLinqConsole
    {
        private readonly ILinqService _linqService;
        private readonly OutputFormatter _outputFormatter;

        public CollectionAndLinqConsole(ILinqService linqService)
        {
            _linqService = linqService;
            _outputFormatter = new OutputFormatter();
        }

        public async Task Start()
        {
            await _linqService.InitAsync();
            ShowMenu();
        }

        public void ShowMenu()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Press digit for item chosing:");
            _outputFormatter.List(this.ShowMenu,
                ("Show tasks amount in each project by an user id", () => ShowTaskAmountInProjectDialog()),
                ("Show tasks where a task name length less than 45 by an user id", () => ShowTaskByTaskNameLengthDialog()),
                ("Show finished in 2020 tasks by an userid", () => ShowUsersFinishedTasksDialog()),
                ("Show teams with members where members more than 10 years old", () => ShowTeamsWithMembers()),
                ("Show sorted alphabetically by name users list with descended sorted tasks by name length lists",
                    () => ShowUsersWithTasks()),
                ("Show an user's analitics data by id", () => ShowUsersAnaliticsData()),
                ("Show projects' analitics data", () => ShowProjectsAnaliticsData()));
        }

        private void ShowTaskAmountInProjectDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show tasks amount in each project by an user id");

            var userId = GetEnteredId();

            var result = _linqService.GetPerformersTasksCountByProjects(userId.Value);
            foreach (var item in result)
            {
                _outputFormatter.ListItem($"{item.Key.Name} - {item.Value}");
            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowTaskByTaskNameLengthDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show tasks where a task name length less than 45 by an user id");

            var userId = GetEnteredId();

            var result = _linqService.GetPerformersTasksByNameLength(userId.Value);
            foreach (var item in result)
            {
                _outputFormatter.ListItem($"Task Id: {item.Id}\n\tTask name: {item.Name}\n\tPerformer Id {item.PerformerId}\n");
            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowUsersFinishedTasksDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show finished in 2020 tasks by an userid");

            var userId = GetEnteredId();

            var result = _linqService.GetPerformersFinishedTasksByYear(userId.Value);
            foreach (var item in result)
            {
                _outputFormatter.ListItem($"Task Id: {item.Item1}\n\tTask name: {item.Item2}\n");
            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowTeamsWithMembers()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show teams with members where members more than 10 years old");

            var result = _linqService.GetTeamsAndMembersFilteredByMembersAge();
            foreach (var item in result)
            {
                _outputFormatter.ListItem($"Team Id: {item.Item1}\n\tTeam name: {item.Item2}\n");

                foreach(var user in item.Item3)
                {
                    _outputFormatter.ListItem($"\tUser Id: {user.Id} - User name: {user.FirstName} {user.LastName}\n");
                }
            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowUsersWithTasks()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show sorted alphabetically by name users list with descended sorted tasks by name length lists");

            var result = _linqService.GetUsersByFirstNameWithTasksSortedByNameLength();
            foreach (var item in result)
            {
                _outputFormatter.ListItem($"User Id: {item.Item1.Id} - User name: {item.Item1.FirstName} {item.Item1.LastName}\n");

                foreach (var task in item.Item2)
                {
                    _outputFormatter.ListItem($"\tTask Id: {task.Id} - Task name: {task.Name}\n");
                }
            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowUsersAnaliticsData()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show an user's analitics data by id");
            var userId = GetEnteredId();

            var result = _linqService.GetUserAnaliticInfo(userId.Value);

            if (result == null)
            {
                _outputFormatter.Info($"There is no user with id - {userId}");
            }
            else
            {
                _outputFormatter.ListItem($"User Id: {result.User.Id} - User name: {result.User.FirstName} {result.User.LastName}");

                if (result.LastProject != null)
                    _outputFormatter.ListItem($"Last project Id: {result.LastProject.Id} - Last project name: {result.LastProject.Name}\n\t" +
                    $"Last project tasks count: {result.LastProjectTasksAmount}\n\t" +
                    $"User canceled or not finished task count: {result.CanceledOrNotDoneTasksAmount}\n\t" +
                    $"The most time consuming task Id - name: {result.MostTimeConsumingTask.Id} - {result.MostTimeConsumingTask.Name}\n\t");

            }

            _outputFormatter.Quitable();
            ShowMenu();
        }

        private void ShowProjectsAnaliticsData()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show projects' analitics data");

            var result = _linqService.GetProjectsAnaliticInfo();

            foreach(var item in result)
            {
                _outputFormatter.ListItem($"Project Id: {item.Project.Id} - Project name: {item.Project.Name}\n\t" +
                     $"Project tasks count in projects with description length more than 20 characters \n\t\tor tasks amount less than 3 : {item.MembersCountByTasksCountAndProjDescLength}");


                if (item.Project.Tasks.Count > 0)
                    _outputFormatter.ListItem($"Task with the shortest name Id - name: {item.TaskWithShortestName.Id} - {item.TaskWithShortestName.Name}\n\t" +
                                        $"Task with the longest description Id - name: {item.TaskWithLongestDescription.Id} - {item.TaskWithLongestDescription.Name}\n\t");

            }
           
            _outputFormatter.Quitable();
            ShowMenu();
        }

        private int? GetEnteredId()
        {
            var wasIdParsed = false;
            var id = 0;

            while (!wasIdParsed || id < 0)
            {
                wasIdParsed = Int32.TryParse(_outputFormatter.Input("Id - $"), out id);
                if (!wasIdParsed || id < 0)
                {
                    _outputFormatter.CenteredText("You have entered invalid id.", Color.Red);
                    _outputFormatter.Quitable();
                }
            }

            return wasIdParsed ? (int?)id : null;
        }
    }
}
