﻿using AutoMapper;
using CollectionsAndLinq.Interfaces;
using CollectionsAndLinq.MappingProfiles;
using CollectionsAndLinq.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace CollectionsAndLinq
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        static async Task Main(string[] _)
        {
            RegisterServices();

            var console = _serviceProvider.GetService<ICollectionAndLinqConsole>();
            await console.Start();

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection()
                .AddScoped<IMapper>(p => new Mapper(
                     new MapperConfiguration(cfg =>
                     {
                         cfg.AddProfile<ProjectProfile>();
                         cfg.AddProfile<TaskProfile>();
                         cfg.AddProfile<TeamProfile>();
                         cfg.AddProfile<UserProfile>();
                     })))
                .AddSingleton<IApiService, ApiService>()
                .AddSingleton<ILinqService>(p => new LinqService(
                    p.GetRequiredService<IApiService>(), 
                    p.GetRequiredService<IMapper>()))
                .AddSingleton<ICollectionAndLinqConsole>(p => new CollectionAndLinqConsole(
                    p.GetRequiredService<ILinqService>()));

            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}