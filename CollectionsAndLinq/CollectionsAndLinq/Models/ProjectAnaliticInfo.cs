﻿namespace CollectionsAndLinq.Models
{
    public class ProjectAnaliticInfo
    {
        public Project Project { get; set; }
        public ProjectTask TaskWithLongestDescription { get; set; }
        public ProjectTask TaskWithShortestName { get; set; }
        public int MembersCountByTasksCountAndProjDescLength { get; set; }
    }
}
