﻿namespace CollectionsAndLinq.Models
{
    public enum TaskState : byte
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}