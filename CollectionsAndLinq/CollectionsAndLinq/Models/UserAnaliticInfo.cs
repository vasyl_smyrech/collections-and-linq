﻿namespace CollectionsAndLinq.Models
{
    public class UserAnaliticInfo
    {
        public User User { get; set; }
        public Project LastProject {get; set; }
        public int LastProjectTasksAmount { get; set; }
        public int CanceledOrNotDoneTasksAmount { get; set; }
        public ProjectTask MostTimeConsumingTask { get; set; }
    }
}
