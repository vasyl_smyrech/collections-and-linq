﻿using Newtonsoft.Json;
using System;

namespace CollectionsAndLinq.DTOs
{
    public class UserDto
    {
        [JsonProperty("id", Required = Required.Always)]
        public int Id { get; set; }

        [JsonProperty("firstName", Required = Required.AllowNull)]
        public string FirstName { get; set; }

        [JsonProperty("lastName", Required = Required.AllowNull)]
        public string LastName { get; set; }

        [JsonProperty("email", Required = Required.AllowNull)]
        public string Email { get; set; }

        [JsonProperty("birthday", Required = Required.Always)]
        public DateTime Birthday { get; set; }

        [JsonProperty("registeredAt", Required = Required.Always)]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("teamId", Required = Required.AllowNull)]
        public int? TeamId { get; set; }
    }
}
