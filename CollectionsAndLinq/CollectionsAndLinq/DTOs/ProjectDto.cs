﻿using Newtonsoft.Json;
using System;

namespace CollectionsAndLinq.DTOs
{
    public class ProjectDto
    {
        [JsonProperty("id", Required = Required.Always)]
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty("description", Required = Required.AllowNull)]
        public string Description { get; set; }

        [JsonProperty("createdAt", Required = Required.Always)]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline", Required = Required.Always)]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId", Required = Required.Always)]
        public int AuthorId { get; set; }

        [JsonProperty("teamId", Required = Required.Always)]
        public int TeamId { get; set; }
    }
}
