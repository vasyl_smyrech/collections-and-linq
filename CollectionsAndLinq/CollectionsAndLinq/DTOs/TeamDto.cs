﻿using Newtonsoft.Json;
using System;

namespace CollectionsAndLinq.DTOs
{
    public class TeamDto
    {
        [JsonProperty("id", Required = Required.Always)]
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty("createdAt", Required = Required.Always)]
        public DateTime CreatedAt { get; set; }
    }
}
