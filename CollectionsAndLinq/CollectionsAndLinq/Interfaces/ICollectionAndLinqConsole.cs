﻿using System.Threading.Tasks;

namespace CollectionsAndLinq.Interfaces
{
    public interface ICollectionAndLinqConsole
    {
        Task Start();
    }
}
