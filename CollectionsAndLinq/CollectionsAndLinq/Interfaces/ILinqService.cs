﻿using CollectionsAndLinq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Interfaces
{
    public interface ILinqService
    {
        Task InitAsync();
        public Dictionary<Project, int> GetPerformersTasksCountByProjects(int performerId);
        public List<ProjectTask> GetPerformersTasksByNameLength(int performerId);
        public List<(int, string)> GetPerformersFinishedTasksByYear(int performerId);
        public List<(int, string, List<User>)> GetTeamsAndMembersFilteredByMembersAge();
        public List<(User, List<ProjectTask>)> GetUsersByFirstNameWithTasksSortedByNameLength();
        public UserAnaliticInfo GetUserAnaliticInfo(int userId);
        public List<ProjectAnaliticInfo> GetProjectsAnaliticInfo();
    }
}
