﻿using CollectionsAndLinq.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Interfaces
{
    public interface IApiService
    {
        Task<List<ProjectDto>> GetProjectsAsync();
        Task<ProjectDto> GetProjectByIdAsync(int projectId);
        Task<List<TaskDto>> GetTasksAsync();
        Task<TaskDto> GetTaskByIdAsync(int taskId);
        Task<List<UserDto>> GetUsersAsync();
        Task<UserDto> GetUserByIdAsync(int userId);
        Task<List<TeamDto>> GetTeamsAsync();
        Task<TeamDto> GetTeamByIdAsync(int teamId);
    }
}
