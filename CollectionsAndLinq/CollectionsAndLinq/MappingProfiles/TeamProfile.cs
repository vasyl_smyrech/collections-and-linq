﻿using AutoMapper;
using CollectionsAndLinq.DTOs;
using CollectionsAndLinq.Models;

namespace CollectionsAndLinq.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDto, Team>();
        }
    }
}
