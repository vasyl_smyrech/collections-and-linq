﻿using AutoMapper;
using CollectionsAndLinq.DTOs;
using CollectionsAndLinq.Models;

namespace CollectionsAndLinq.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDto, ProjectTask>();
        }
    }
}
