﻿using AutoMapper;
using CollectionsAndLinq.DTOs;
using CollectionsAndLinq.Models;

namespace CollectionsAndLinq.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDto, Project>();
        }
    }
}
