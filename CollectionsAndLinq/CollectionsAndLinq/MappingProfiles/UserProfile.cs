﻿using AutoMapper;
using CollectionsAndLinq.DTOs;
using CollectionsAndLinq.Models;

namespace CollectionsAndLinq.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>();
        }
    }
}
