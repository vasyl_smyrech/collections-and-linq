﻿using AutoMapper;
using CollectionsAndLinq.Interfaces;
using CollectionsAndLinq.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Services
{
    public class LinqService : ILinqService
    {
        private readonly IApiService _apiService;
        private readonly IMapper _mapper;
        private List<Project> _projects;
        private List<ProjectTask> _tasks;
        private List<User> _users;
        private List<Team> _teams;
        public LinqService(IApiService apiService, IMapper mapper)
        {
            _apiService = apiService;
            _mapper = mapper;
        }
        
        public async Task InitAsync()
        {
            _projects = _mapper.Map<List<Project>>(await _apiService.GetProjectsAsync());
            _teams = _mapper.Map<List<Team>>(await _apiService.GetTeamsAsync());
            _tasks = _mapper.Map<List<ProjectTask>>(await _apiService.GetTasksAsync());
            _users = _mapper.Map<List<User>>(await _apiService.GetUsersAsync());

            _projects = _projects
                .Join(_users, p => p.AuthorId, u => u.Id, (p, u) =>
                {
                    p.Author = u;
                    return p;
                })
                .Join(_teams, p => p.TeamId, t => t.Id, (p, t) =>
                {
                    p.Team = t;
                    return p;
                })
                .GroupJoin(_tasks, p => p.Id, t => t.ProjectId,
                    (project, taskCollection) =>
                    {
                        project.Tasks =
                            taskCollection.Join(_users, t => t.PerformerId, u => u.Id,
                                (t, u) =>
                                {
                                    t.Performer = u;
                                    return t;
                                }
                            ).ToList();
                        return project;
                    }).ToList();
        }

        public Dictionary<Project, int> GetPerformersTasksCountByProjects(int authorId)
        {
            return _projects
                .Where(p => p.AuthorId == authorId)
                .ToDictionary(p => p, p => p.Tasks.Count);
        }

        public List<ProjectTask> GetPerformersTasksByNameLength(int performerId)
        {
            return _tasks
                .Where(t => t.PerformerId == performerId && t.Name.Length < 45)
                .ToList();
        }

        public List<(int, string)> GetPerformersFinishedTasksByYear(int performerId)
        {
            return _tasks
                .Where(t => t.PerformerId == performerId && t.FinishedAt.Year == 2020)
                .Select(t => (t.Id, t.Name))
                .ToList();
        }

        public List<(int, string, List<User>)> GetTeamsAndMembersFilteredByMembersAge()
        {
            return _teams.Join(
                _users
                    .Where(m => m.Birthday < System.DateTime.Now.AddYears(-10) && m.TeamId != null)
                    .OrderByDescending(m => m.RegisteredAt)
                    .GroupBy(m => m.TeamId)
                    .ToDictionary(g => g.Key, g => g.ToList()),
                t => t.Id,
                g => g.Key,
                (t, g) => (t.Id, t.Name, g.Value)
                ).ToList();
        }

        public List<(User, List<ProjectTask>)> GetUsersByFirstNameWithTasksSortedByNameLength()
        {
            return _users
                .OrderBy(u => u.FirstName)
                .GroupJoin(_tasks, u => u.Id, t => t.PerformerId,
                    (user, usersTasks) => (user, usersTasks.OrderByDescending(t => t.Name.Length).ToList()))
                .ToList();
        }

        public UserAnaliticInfo GetUserAnaliticInfo(int userId)
        {
            return _users
                .Where(u => u.Id == userId)
                .GroupJoin(_tasks, u => u.Id, t => t.PerformerId, (user, userTasks) => (user, userTasks))
                .GroupJoin(_projects, (data) => data.user.Id, p => p.AuthorId,
                    (data, userProjects) =>
                    {
                        var lastProject = userProjects.OrderByDescending(p => p.CreatedAt).FirstOrDefault();

                        return new UserAnaliticInfo()
                        {
                            User = data.user,
                            LastProject = lastProject,
                            LastProjectTasksAmount = lastProject == null ? 0 : lastProject.Tasks.Count,
                            CanceledOrNotDoneTasksAmount = data.userTasks.Count(t => t.State == TaskState.Canceled || t.State == TaskState.InProgress),
                            MostTimeConsumingTask = _tasks.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault()
                        };
                    }
                    ).FirstOrDefault();
        }

        public List<ProjectAnaliticInfo> GetProjectsAnaliticInfo()
        {
            return _projects.Select(p => new ProjectAnaliticInfo()
            {
                Project = p,
                TaskWithLongestDescription = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                TaskWithShortestName = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                MembersCountByTasksCountAndProjDescLength =
                    p.Description.Length > 20 || p.Tasks.Count < 3 ? p.Tasks.Select(t => t.Performer).Distinct().Count() : 0
            }).ToList();
        }
    }
}
