﻿using CollectionsAndLinq.DTOs;
using CollectionsAndLinq.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Services
{
    public class ApiService : IApiService
    {
        private static readonly HttpClient _client = new HttpClient();

        static ApiService()
        {
            _client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/api/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<ProjectDto>> GetProjectsAsync()
        {
            var response = _client.GetAsync("projects").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);
            var allProjectsJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ProjectDto>>(allProjectsJsonString);
        }

        public async Task<ProjectDto> GetProjectByIdAsync(int projectId)
        {
            var response = _client.GetAsync($"projects/{projectId}").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var projectJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ProjectDto>(projectJsonString);
        }

        public async Task<List<TaskDto>> GetTasksAsync()
        {
            var response = _client.GetAsync("tasks").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var allTasksJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<TaskDto>>(allTasksJsonString);
        }

        public async Task<TaskDto> GetTaskByIdAsync(int taskId)
        {
            var response = _client.GetAsync($"projects/{taskId}").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var taskJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TaskDto>(taskJsonString);
        }

        public async Task<List<UserDto>> GetUsersAsync()
        {
            var response = _client.GetAsync("users").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var allUsersJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<UserDto>>(allUsersJsonString);
        }

        public async Task<UserDto> GetUserByIdAsync(int userId)
        {
            var response = _client.GetAsync($"users/{userId}").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var userJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserDto>(userJsonString);
        }

        public async Task<List<TeamDto>> GetTeamsAsync()
        {
            var response = _client.GetAsync("teams").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var allTeamsJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<TeamDto>>(allTeamsJsonString);
        }

        public async Task<TeamDto> GetTeamByIdAsync(int teamId)
        {
            var response = _client.GetAsync($"teams/{teamId}").Result;
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var teamJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TeamDto>(teamJsonString);
        }

        private async Task ThrowException(HttpResponseMessage response)
        {
            throw new Exception($"Response code - {response.StatusCode}" +
          $"\n\t{await response.Content.ReadAsStringAsync()}");
        }
    }
}
